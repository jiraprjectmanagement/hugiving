

<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->



        
<!-- Main Banner  -->

<div class="main-wraper impact-page">
    
    <!-- Menu -->
    <?php include './include/menu.php' ?>
    <!-- Menu -->

    <div class="vide-area">
        <div class="container-fluid hs-set">
            <div class="row justify-content-center">
                <!-- <div class="col-lg-12">
                    <div class="vid-iner">
                        <a href="https://www.youtube.com/watch?v=Ks9Hm1GR03M" data-fancybox="images" data-aos="zoom-in" data-aos-duration="1000">
                            <img src="img/play-icon-white.png" alt="" class="video-icon">
                        </a>
                    </div>
                </div> -->
                <div class="col-xl-5 col-lg-8 p-0 col-12">
                    <!-- <div class="empower-text">
                        <h2><span>Empowering</span> our Next Generation Leader</h2>
                        <a href="javascript:;" class="find-out">Find Out How</a>
                    </div> -->
                    <!-- <a href="#more" class="down-arrow">
                        <img src="img/down-arrow.svg" alt="">
                    </a> -->
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Main Banner  -->





<!-- Impact Stores -->
<section class="impact-stories impact-page-sec">
    <h1 class="sec-heading">Impact Stories</h1>
    <div class="container">
        <div class="row coomon-responsove-impact-slider">
            <div class="col-lg-3">
                <div class="stud-box" data-aos="zoom-in" data-aos-duration="1000">
                    <a href="https://www.youtube.com/watch?v=jbYcB7SYmT0" data-fancybox="images" class="vid-set">
                        <img src="img/stud.webp" alt="" class="img-fluid">
                        <div class="stud-vid">
                               <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                            <p> 
                            <b> Osama Yousuf:</b> <br>
                                Research Abroad 
                                Scholar
                                 </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="stud-box" data-aos="zoom-in" data-aos-duration="1200">
                    <a href="https://www.youtube.com/watch?v=Mr9WI6gZhFs" data-fancybox="images" class="vid-set">
                        <img src="img/stud1.webp" alt="" class="img-fluid">
                        <div class="stud-vid">
                            <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                            <p> <b>Mashal
                                Faraz Shamsi:</b> Pathfinders of 
                                Tomorrow</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="stud-box" data-aos="zoom-in" data-aos-duration="1500">
                    <a href="https://www.youtube.com/watch?v=2E-jqEpCst8" data-fancybox="images" class="vid-set">
                        <img src="img/stud2.webp" alt="" class="img-fluid">
                        <div class="stud-vid"> <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                            <p> <b>Fatima  
                                Moin:</b><br> Pathfinders of 
                                Tomorrow</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="stud-box" data-aos="zoom-in" data-aos-duration="1200">
                    <a href="https://www.youtube.com/watch?v=6z6rXTF2mAM" data-fancybox="images" class="vid-set">
                        <img src="img/stud3.webp" alt="" class="img-fluid">
                        <div class="stud-vid">
                            <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                            <p><b>Maisam Hyder: </b><br>
                             HU senior receives an
                            extra ordinary honor</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Impact Stores -->


<!-- Give Now -->
<?php include './include/give-now.php' ?>
<!-- Give Now -->


<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->

