

<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->

<!-- News Modal -->
<?php include './include/new-modal.php' ?>
<!-- News Modal -->



<!-- Main Banner  -->

<div class="main-wraper home-wrap">
     
    <!-- Menu -->
        <?php include './include/menu.php' ?>
    <!-- Menu -->

    <div class="overlay"></div>

    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="img/videos/HUFS-US-slider-video.mp4" type="video/mp4">
    </video>
    
    <div class="vide-area">
        <div class="container-fluid hs-set">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="vid-iner">
                        <a data-fancybox data-src="#one" class="fancybox">
                            <img src="img/play-icon-white.png" alt="" class="video-icon">
                            <span class="intro-banner-vdo-play-btn">
                                <span class="ripple pinkBg"></span>
                                <span class="ripple pinkBg"></span>
                                <span class="ripple pinkBg"></span>
                            </span>
                            <div id="one" style="display:none">
                                <video id='myVideo' width="100%" height="auto" controls muted>
                                <source src="img/videos/HUFS-US-slider-video.mp4" type="video/mp4">          
                                Your browser does not support HTML5 videos.
                                </video>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-8 p-0 col-12">
                    <!-- <div class="empower-text">
                        <h2><span>Empowering</span> our Next Generation Leader</h2>
                        <a href="javascript:;" class="find-out">Find Out How</a>
                    </div> -->
                    <a href="#more" class="down-arrow">
                        <img src="img/down-arrow.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Main Banner  -->



<!-- Para Area  -->
<section class="para-ara" id="more">
    <div class="container">
        <div class="para-ara-con" data-aos="fade-up" data-aos-duration="1000">
            <p>Welcome to Giving at Habib University. Your gifts ensure that students in Pakistan are able to access an extraordinary liberal arts education, which transforms them into thoughtful, engaged citizens. Join us in our mission to prepare the next generation of leaders, who will change the world. With your help, Habib University can continue to make an impact on the lives of youth.</p>
            <a href="about" class="lrn-more">Learn More</a>
        </div>
    </div>
</section>
<!-- Para Area  -->


 <!-- Community Area  -->
<section class="community-area">
    <h1 class="sec-heading">Our Community</h1>
    <div class="commun-inner" data-aos="fade-up" data-aos-duration="1500">
            <div class="container">
                <div class="row community-slider owl-theme owl-carousel">
                    <div class="commun-box">
                        <a href="https://www.youtube.com/watch?v=P13khja3qtA" data-fancybox="images" class="vide-comun-bx-set">
                            <img src="img/comunbox.png" alt="" class="img-fluid">
                        </a>
                        <h5>Bashir Ali Mohammad</h5>
                        <!-- <p>CEO Babson Global</p> -->
                    </div>
                    <div class="commun-box">
                        <a href="https://www.youtube.com/watch?v=MVwjJ0K4hB0" data-fancybox="images" class="vide-comun-bx-set">
                            <img src="img/comunbox1.png" alt="" class="img-fluid">
                        </a>
                        <h5>Shahbaz Yasin Malik</h5>
                        <!-- <p>President Habib Bank AG Zurich</p> -->
                    </div>
                    <div class="commun-box" >
                        <a href="javascript:;" class="vide-comun-bx-set">
                             <img src="img/comunbox2.png" alt="" class="img-fluid">
                        </a>
                        <h5>Shoukat Ali Dhanani</h5>
                        <!-- <p>CEO, The Dhanani Group</p> -->
                    </div>
                    <div class="commun-box">
                        <a href="https://www.youtube.com/watch?v=P13khja3qtA" data-fancybox="images" class="vide-comun-bx-set">
                            <img src="img/comunbox.png" alt="" class="img-fluid">
                        </a>
                        <h5>Bashir Ali Mohammad</h5>
                        <!-- <p>CEO Babson Global</p> -->
                    </div>
                    <div class="commun-box">
                        <a href="https://www.youtube.com/watch?v=MVwjJ0K4hB0" data-fancybox="images" class="vide-comun-bx-set">
                            <img src="img/comunbox1.png" alt="" class="img-fluid">
                        </a>
                        <h5>Shahbaz Yasin Malik</h5>
                        <!-- <p>President Habib Bank AG Zurich</p> -->
                    </div>
                    <div class="commun-box" data-aos="fade-up" data-aos-duration="1500">
                        <a href="javscript:;"  class="vide-comun-bx-set">
                             <img src="img/comunbox2.png" alt="" class="img-fluid">
                        </a>
                        <h5>Shoukat Ali Dhanani</h5>
                        <!-- <p>CEO, The Dhanani Group</p> -->
                    </div>
                </div>
                    <a href="our-community" class="view-all">View All</a>
            </div>
    </div>
</section>
 <!-- Community Area  -->



<!-- Impact Stores -->
<section class="impact-stories impact-bg">
    <div class="container">
        <div class="row resp-column-reverse">
            <div class="col-lg-8" data-aos="fade-right" data-aos-duration="1500">
                <div class="row coomon-responsove-impact-slider">
                    <div class="col-lg-4">
                        <div class="stud-box">
                            <a href="https://www.youtube.com/watch?v=TRsEmyBfrKI" data-fancybox="images" class="vid-set">
                                <img src="img/stud.png" alt="" class="img-fluid">
                                <div class="stud-vid">
                                       <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                                    <p> 
                                    <b>Osama Yousuf:</b> <br>
                                        Research Abroad 
                                        Scholar 
                                        </p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="stud-box">
                            <a href="https://www.youtube.com/watch?v=nefNtliXJk8" data-fancybox="images" class="vid-set">
                                <img src="img/stud1.png" alt="" class="img-fluid">
                                <div class="stud-vid">
                                    <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                                    <p> <b>Mashal Faraz Shamsi:</b>
                                        Pathfinders of 
                                        Tomorrow</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="stud-box">
                            <a href="https://www.youtube.com/watch?v=0rSKv-xYUHU" data-fancybox="images" class="vid-set">
                                <img src="img/stud2.png" alt="" class="img-fluid">
                                <div class="stud-vid"> <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                                    <p><b>Fatima Moin:</b>
                                         Pathfinders of 
                                        Tomorrow </p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4"  data-aos="fade-left" data-aos-duration="1500">
                <div class="impact-right">
                    <h1 class="sec-heading">Impact Stories</h1>
                    <p>Now, more than endlessly, your support makes a real difference in our students' lives. Your generous gifts provide bright, promising, hard-working students the opportunity to fulfill their educational goals</p>
                    <a href="impact" class="lrn-more white">View More</a>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- Impact Stores -->

<!-- Way Of Giving -->
<section class="way-giving">
    <h1 class="sec-heading">Ways of Giving</h1>
    <div class="container">
        <div class="giving-ways-main no-gutters" data-aos="fade-right" data-aos-duration="1500">
            <div class="giving-ways-box">
                <!-- <a href="student-support" class="way-clicks"> </a> -->
                    <div class="giving-box">
                        <div class="iner-gv-box">
                            <img src="img/giving-box.png" alt="" class="img-fluid"/>
                            <div class="giving-text">
                                <h3>Student Support</h3>
                                <a href="student-support" class="give-read">Read More</a>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="giving-ways-box">
                <!-- <a href="create-legacy" class="way-clicks"></a> -->
                <div class="giving-box">
                    <div class="iner-gv-box">
                        <img class="des-show" src="img/giving-box1.png" alt="" class="img-fluid">
                        <img class="des-none" src="img/giving-box5.png" alt="" class="img-fluid">
                        
                        <div class="giving-text">
                            <h3>Create your own legacy</h3>
                                <a href="create-legacy" class="give-read">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="giving-ways-box">
                <!-- <a href="intellectual-resources" class="way-clicks"></a>  -->
                <div class="giving-box">
                    <div class="iner-gv-box">
                        <img class="des-show" src="img/giving-box2.png" alt="" class="img-fluid">
                        <img class="des-none" src="img/giving-box4.png" alt="" class="img-fluid">
                        <div class="giving-text">
                            <h3>Intellectual Resources</h3>
                            <a href="intellectual-resources" class="give-read">Read More</a>
                        </div>
                    </div>
                </div>   
            </div>
            <div class="giving-ways-box">
                    <!-- <a href="hu-reach" class="way-clicks"></a> -->
                    <div class="giving-box">
                        <div class="iner-gv-box">
                            <img src="img/giving-box3.png" alt="" class="img-fluid">
                            <div class="giving-text">
                                <h3>Become an HU reach <br>
                                    Ambassador</h3>
                                <a href="hu-reach" class="give-read">Read More</a>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</section>
<!-- Way Of Giving -->



<section class="new-area">
    <div class="new-inner">
        <div class="container">
            <h1 class="sec-heading">In The News</h1>
            <div class="row news-slider  owl-theme owl-carousel" data-aos="fade-up" data-aos-duration="1500">
                <a data-toggle="modal" data-target="#newsone" href="https://habib.edu.pk/HU-news/khawaja-mashooqullah-music-room-celebrating-the-splendor-of-south-asian-music/" target="_blank" class="article-links">
                    <div class="item-new">
                        <img src="img/new1.jpg" alt="" class="img-fluid">
                        <p>Khawaja Mashooqullah  <br>
                            Music Room: Celebrating the   <br>
                            Splendor of South Asian Music</p>
                    </div>
                </a>
                <a data-toggle="modal" data-target="#newstwo" href="https://habib.edu.pk/HU-news/ceo-of-habibmetro-bank-applauds-habibs-need-blind-admission-policy/" target="_blank" class="article-links">
                    <div class="item-new">
                        <img src="img/new2.jpg" alt="" class="img-fluid">
                        <p>CEO of HabibMetro Bank applauds  <br>
                            Habib’s Need-Blind Admission Policy</p>
                    </div>
                </a>
                <a data-toggle="modal" data-target="#newsthree" href="https://habib.edu.pk/HU-news/community-ownership-essential-for-improvement-of-higher-education/" target="_blank" class="article-links">
                    <div class="item-new">
                        <img src="img/new3.jpg" alt="" class="img-fluid">
                        <p>Community Ownership Essential <br>
                            for Improvement of Higher <br>
                            Education</p>
                    </div>
                </a>
                <a data-toggle="modal" data-target="#newsone" href="https://habib.edu.pk/HU-news/khawaja-mashooqullah-music-room-celebrating-the-splendor-of-south-asian-music/" target="_blank" class="article-links">
                    <div class="item-new">
                        <img src="img/new1.jpg" alt="" class="img-fluid">
                        <p>Khawaja Mashooqullah  <br>
                            Music Room: Celebrating the   <br>
                            Splendor of South Asian Music</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>



<!-- Give Now -->
<?php include './include/give-now.php' ?>
<!-- Give Now -->


<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->
