

<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->




        
<!-- Main Banner  -->

<div class="main-wraper intellactual">
    
    <!-- Menu -->
    <?php include './include/menu.php' ?>
    <!-- Menu -->

   <div class="student-sup-in">
       <div class="banner-content">
           <h1>Ways Of Giving</h1>
           <h2>Intellectual Resources</h2>
       </div>
      
   </div>

</div>
<!-- Main Banner  -->


<div class="serprator"></div>

<!-- Common Scholership area -->
<section class="common-shollership-area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 pr-0 p-xs-0">
                <div class="scholer-conternt inte-scho-fs">
                   <h1>Endowed Program</h1>
                   <p> Through endowing specific programs you can allow Habib to develop stronger teaching programs, invest in new technologies, and maintain laboratories and other physical assets.</p>
                </div>
            </div>
            <div class="col-lg-4 pl-0 p-xs-0">
                <div class="scholer-images">
                    <img src="img/intellactual/enodowed-program-image.webp" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Common Scholership area -->


<!-- Common Scholership area -->
<section class="common-shollership-area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4 pr-0 p-xs-0">
                <div class="scholer-images">
                    <img src="img/intellactual/endowed-chair-image.webp" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-8 pl-0 p-xs-0">
                <div class="scholer-conternt inte-scho">
                   <h1> Endowed Chair</h1>
                   <p>Habib University’s faculty is globally renowned and truly committed to our vision of redefining education as an intellectual experience. The majority of faculty at Habib University have worked or studied abroad, and choose to return to Pakistan to mentor students. </p>

                    <p>Your contribution can enable Habib to continue to attract, retain, and honor distinguished faculty members. </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Common Scholership area -->


<!-- Make Gift -->
<?php include './include/make-gift.php' ?>
<!-- Make Gift -->

<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->
