

<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->




        
<!-- Main Banner  -->

<div class="main-wraper commun-banner">
     
    <!-- Menu -->
    <?php include './include/menu.php' ?>
    <!-- Menu -->

   <div class="student-sup-in">
       <div class="banner-content">
           <!-- <h1>Our Community</h1> -->
       </div>
   </div>

</div>
<!-- Main Banner  -->


<!-- Para Area  -->
<section class="para-ara commun-para" id="more">
    <div class="container">
        <div class="para-ara-con">
            <p>Our community is the pillar of Habib University, facilitating the elevation of higher education in Pakistan and expressing faith in our vision to provide an excellent university education to high-merit students. Learn more about the stories of our supporters and become a part of our community today.</p>
        </div>
    </div>
</section>
<!-- Para Area  -->

<!-- Impact Stores -->
<section class="impact-stories our-com-impac">
    <div class="container">
        <div class="row responsove-our-comun-slider load-more-script">
            <div class="col-lg-4">
                <div class="stud-box" data-aos="zoom-in" data-aos-duration="1000">
                    <a href="https://www.youtube.com/watch?v=P13khja3qtA" data-fancybox="images" class="vid-set">
                        <img src="img/community/image1.webp" alt="" class="img-fluid">
                        <div class="stud-vid">
                               <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                            <p> 
                           <b> Generosity in Focus :</b> <br>
                            Bashir Ali Mohammad</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="stud-box" data-aos="zoom-in" data-aos-duration="1200">
                    <a href="https://www.youtube.com/watch?v=MVwjJ0K4hB0" data-fancybox="images" class="vid-set">
                        <img src="img/community/image2.webp" alt="" class="img-fluid">
                        <div class="stud-vid">
                            <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                            <p><b>Generosity in Focus : </b><br>
                            Shahbaz Yasin Malik</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="stud-box" data-aos="zoom-in" data-aos-duration="1500">
                    <a href="javascript:;" class="vid-set">
                        <img src="img/community/image3.webp" alt="" class="img-fluid">
                        <div class="stud-vid"> <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                            <p><b>Generosity in Focus :</b> <br>
                            Shoukat Ali Dhanani</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="stud-box" data-aos="zoom-in" data-aos-duration="1000">
                    <a href="javascript:;" class="vid-set">
                        <img src="img/community/image4.webp" alt="" class="img-fluid">
                        <div class="stud-vid">
                               <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                            <p> 
                            <b>Generosity in Focus :</b><br>
                               </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="stud-box" data-aos="zoom-in" data-aos-duration="1200">
                    <a href="javascript:;" class="vid-set">
                        <img src="img/community/image4.webp" alt="" class="img-fluid">
                        <div class="stud-vid">
                            <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                            <p><b>Generosity in Focus :</b> <br>
                                </p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="stud-box" data-aos="zoom-in" data-aos-duration="1500">
                    <a href="javascript:;" class="vid-set">
                        <img src="img/community/image4.webp" alt="" class="img-fluid">
                        <div class="stud-vid"> <img src="img/icon-stud.png" alt="" class="icon-stud img-fluid">
                            <p><b>Generosity in Focus :</b> <br>
                               </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <a href="javascript:;" class="lrn-more load-more">View More</a>
    </div>
</section>
<!-- Impact Stores -->


<!-- Give Now -->
<?php include './include/give-now.php' ?>
<!-- Give Now -->



<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->
