
//   Home Page JS

$(function () {

    $(".dropdown").click(function(){
        $(this).addClass("show");
     });


    //  Fancy Box Autoplay Video
    // $(".fancybox").fancybox({
    //     afterShow: function() {
    //     // After the show-slide-animation has ended - play the vide in the current slide
    //     this.content.find('video').trigger('play')
        
    //     // Attach the ended callback to trigger the fancybox.next() once the video has ended.
    //     this.content.find('video').on('ended', function() {
    //         $.fancybox.next();
    //     });
    //     }
    // });
    //  Fancy Box Autoplay Video


    $(document).ready(function() {
        $("[data-fancybox]").fancybox({
          afterShow: function() {
            // After the show-slide-animation has ended - play the vide in the current slide
           var vid = document.getElementById("myVideo"); 
           vid.play(); 
    
            // Attach the ended callback to trigger the fancybox.next() once the video has ended.
            this.content.find('video').on('ended', function() {
              $.fancybox.next();
            });
          }
        });
      });
     
$('.community-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    dots: false,
    margin:60,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: false,
            stagePadding: 15,
            margin:20,
        },                   
        574:{
            items:2,
            dots: false,
            margin:30,
            stagePadding: 10,
        },
        766: {
            items: 3,
            dots: false,
            margin:30,
        }, 
        992: {
            items: 3,
            dots: false,
            margin:30,
        }
    }
});

   

$('.news-slider').owlCarousel({
    loop:false,
    nav: true,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    dots: false,
    margin:30,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    mouseDrag: true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            dots: false,
        },                   
        574:{
            items:2,
            dots: false,
        },
        766: {
            items: 2,
            dots: false,
        }, 
        990: {
            items: 3,
            dots: false
        }
    }
});


if($(window).width() <= 768){
    if(('.coomon-responsove-slider').length != 0){
        $('.coomon-responsove-slider').addClass('owl-carousel owl-theme');
        $('.coomon-responsove-slider').owlCarousel({
            loop:false,
            margin:0,
            dots: true,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            autoplay:false,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1
                }, 
                480:{

                    items:1
                },
                577:{

                    items:1
                },
                767:{
                    items:1
                }
            }
        });
    }
}

if($(window).width() <= 768){
    if(('.coomon-responsove-impact-slider').length != 0){
        $('.coomon-responsove-impact-slider').addClass('owl-carousel owl-theme');
        $('.coomon-responsove-impact-slider').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            stagePadding: 5,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1,
                    stagePadding: 6,
                }, 
                480:{

                    items:1,
                    stagePadding: 10,
                },
                574:{

                    items:2,
                    stagePadding: 10,
                },
                767:{
                    items:3
                }
            }
        });
    }
}


if($(window).width() <= 768){
    if(('.responsove-our-comun-slider').length != 0){
        $('.responsove-our-comun-slider').addClass('owl-carousel owl-theme');
        $('.responsove-our-comun-slider').owlCarousel({
            loop:false,
            margin:0,
            nav:false,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
            dots: true,
            autoplay:false,
            stagePadding: 5,
            autoplayTimeout:2000,
            autoplayHoverPause:false,
            mouseDrag: true,
            responsive:{
                0:{

                    items:1
                }, 
                480:{

                    items:1
                },
                574:{

                    items:2
                },
                766:{
                    items:2
                },
                990:{
                    items:3
                }
            }
        });
    }
}
//   Home Page JS



// Student Support Page Js

//   $('.recuring-scholer-slider').slick({
//     dots: true,
//     infinite: true,
//     speed: 300,
//     slidesToShow: 1,
//     centerMode: true,
//     variableWidth: true
//   });
$('.recuring-scholer-slider').owlCarousel({
    stagePadding: 100,
    loop:true,
    margin:46,
    nav:false,
    dots:true,
    responsive:{
        0:{
            items:1,
            stagePadding: 5
        },
        317:{
            items:1,
            stagePadding: 2
        },
        410:{
            items:1,
            stagePadding: 5
        },
        566:{
            items:2,
            stagePadding: 10,
            margin:20
        },
        765:{
            items:2,
            stagePadding: 10,
            margin:20,
        },
        991:{
            items:2,
            margin:20
        },
        1000:{
            items:2
        }
    }
})
// Student Support Page Js



// Impact Page Js

$('.impact-slider').owlCarousel({
    stagePadding: 50,
    loop:true,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    nav: true,
    margin:46,
    dots:false,
    responsive:{
        0:{
            items:1,
            margin:20,
            stagePadding: 3
        },
        373:{
            items:1,
            margin:20
        },
        765:{
            items:2,
            margin:20
        },
        1000:{
            items:3
        }
    }
})
// Impact Page Js





// Global Project Js




$(".nav-main").click(function(){
    $("body").addClass("overflowhidden");
})
$(".closebtn").click(function(){
    $("body").removeClass("overflowhidden");
})





$(".drops-menu").click(function(){
    $(this).toggleClass("drops-menu-anim");
});

$(window).scroll(function(){
    var scrollheader  = $(window).scrollTop() > 50;
    
    if(scrollheader){
        $(".main-head").addClass("header-sticky");
    }
    else{
        $(".main-head").removeClass("header-sticky");
    }
   
});


$(window).scroll(function(){
    var backtotop  = $(window).scrollTop() > 300;
    
    if(backtotop){
        $(".back-top-click").addClass("top-anim");
    }
    else{
        $(".back-top-click").removeClass("top-anim");
    }
   
});


AOS.init({
    disable: function() {
        return window.innerWidth < 994;
    }
});
// Global Project Js






var mY = 0;
$('.recurring-scholership .owl-stage, .impact-page-sec .owl-stage').mousemove(function(e) {

    // moving upward
    if (e.pageX < mY) {
        console.log('Left');
        $(".recurring-scholership .owl-stage, .impact-page-sec .owl-stage").css({"cursor":"url(./img/mouse-hover-icon-right.png), auto"});
        $(".recurring-scholership .owl-stage, .impact-page-sec .owl-stage .vid-set").css({"cursor":"url(./img/mouse-hover-icon-right.png), auto"});
      
    // moving downward
    } else {
        console.log('right');  
        $(".recurring-scholership .owl-stage, .impact-page-sec .owl-stage").css({"cursor":"url(./img/mouse-hover-icon-left.png), auto"});
        $(".recurring-scholership .owl-stage, .impact-page-sec .owl-stage .vid-set").css({"cursor":"url(./img/mouse-hover-icon-left.png), auto"});
    
    }

    // set new mY after doing test above
    mY = e.pageX;

});


$(".load-more-script .col-lg-4").slice(0, 3).show();
$("body").on('click touchstart', '.load-more', function (e) {
    e.preventDefault();
    $(".load-more-script .col-lg-4:hidden").slice(0, 3).slideDown();
    if ($(".load-more-script .col-lg-4:hidden").length == 0) {
        $(".load-more").css('visibility', 'visible');
    }
    // $('html,body').animate({
    //     scrollTop: $(this).offset().top
    // }, 1000);
});


});


function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
  }
  
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}