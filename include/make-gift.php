<!-- Make A Gift -->
<section class="make-gift">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-5">
                <div class="make-content">
                    <h1>How to Make a Gift</h1>
                    <a href="make-a-gift" class="explor-btn">Explore Now</a>
                </div>          
            </div>
        </div>
    </div>
</section>
<!-- Make A Gift -->
