

<!-- Give Now -->
<section class="giving-now">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8" data-aos="fade-up" data-aos-duration="1500">
                <h1 class="sec-heading">Give Now</h1>
                <p>Supporting students, fueling groundbreaking research, advancing solutions to societal 
                    challenges—your gifts to Habib University drive positive change in the world.</p>
                <div class="give-now-inner">
                    <h2>Donate From</h2>
                    <ul class="giv-list">
                        <li> 
                            <a href="https://giving.habib.edu.pk/" target="_blank" class="donate-now"> <img src="img/make-gift/pk.svg" alt="">
                             <h3> Pakistan</h3>
                            </a>
                        </li>
                        <li>
                            <a href="https://giving.hufus.org/" target="_blank" class="donate-now"><img src="img/make-gift/us.svg" alt="">
                             <h3> USA</h3>
                            </a>
                        </li>
                        <li>
                            <a href="https://giving.habibtrust.org.uk/" target="_blank" class="donate-now"><img src="img/make-gift/uk.svg" alt=""> 
                              <h3> UK</h3>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- Give Now -->
