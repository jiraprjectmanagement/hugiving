

<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->



<!-- Main Banner  -->

<div class="main-wraper about-page">


    <!-- Menu -->
    <?php include './include/menu.php' ?>
    <!-- Menu -->

    <div class="vide-area">
        <div class="container-fluid hs-set">
            <div class="row justify-content-center">
                <!-- <div class="col-lg-12">
                    <div class="vid-iner">
                        <a href="https://www.youtube.com/watch?v=Ks9Hm1GR03M" data-fancybox="images" data-aos="zoom-in" data-aos-duration="1000">
                            <img src="img/play-icon-white.png" alt="" class="video-icon">
                        </a>
                    </div>
                </div> -->
                <div class="col-xl-5 col-lg-8 p-0 col-12">
                    <!-- <div class="empower-text">
                        <h2><span>Empowering</span> our Next Generation Leader</h2>
                        <a href="javascript:;" class="find-out">Find Out How</a>
                    </div> -->
                    <!-- <a href="#more" class="down-arrow">
                        <img src="img/down-arrow.svg" alt="">
                    </a> -->
                </div>
            </div>
        </div>
    </div>

</div>
<!-- Main Banner  -->


<section class="about-content-area">
    <div class="container">
        <div class="about-con-inner">
            <h1 class="sec-heading"> About</h1>
            <p>Habib University is Pakistan’s premier, not-for-profit liberal arts and sciences university, catering to talented, high-merit students with a passion for learning and leadership. Habib University fills the intellectual vacuum in higher education, preparing our students to become empathetic, conscious citizens, paving the way for a better future.</p>
            <p>As the country’s first community-owned institution, Habib University provides unconditional access to world-class education to outstanding students from diverse socioeconomic backgrounds. It is only with the generosity of our community of supporters that we are able to fulfill our mission to educate the youth, who are the leaders and innovators of tomorrow.</p>
             <h4>Join us in our endeavor to <br> transform the future of Pakistan</h4>
        </div>
    </div>
</section>

<!-- Give Now -->
<?php include './include/give-now.php' ?>
<!-- Give Now -->

<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->
