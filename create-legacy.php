

<!-- Header -->
<?php include './include/header.php' ?>
<!-- Header -->





<!-- Main Banner  -->

<div class="main-wraper create-legacy">
    
    <!-- Menu -->
      <?php include './include/menu.php' ?>
    <!-- Menu -->

   <div class="student-sup-in">
       <div class="banner-content">
           <h1>Ways Of Giving</h1>
           <h2>Create Your Own Legacy</h2>
       </div>
   </div>

</div>
<!-- Main Banner  -->


<!-- Para Area -->
<section class="create-legacy-para">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="creat-para">
                    <p>A legacy in your Will is the most personal gift you can make. By giving a legacy gift, you make an impact at Habib University that will extend beyond your lifetime to change the lives of generations of Habib students and scholars, all while meeting your philanthropic and financial goals.</p> 
                    <p>A legacy gift enables you to invest in students’ future and preserve wealth for yourself, your loved ones and your <br> immediate and future needs.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Para Area -->

<!-- Common Scholership area -->
<section class="common-shollership-area create-legacy-sup-res">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 pr-0 p-sm-0 p-xs-0">
                <div class="scholer-conternt">
                   <h1>Endowed School</h1>
                   <p> University offers its students a world-class liberal arts education. Our two Schools Dhanani School of Science and Engineering and School of Arts, Humanities & Social Sciences with their unique programs aim to nurture future leaders by providing an education that challenges them intellectual and support them in developing critical and analytical skills.</p>
                   <!-- <a href="#" class="schol-btn">Expand >></a> -->
                </div>
            </div>
            <div class="col-lg-4 pl-0 p-sm-0 p-xs-0">
                <div class="scholer-images">
                    <img src="img/create-legacy/endowed-scool-image.webp" alt="" class="img-fluid">
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Common Scholership area -->


<!-- Common Scholership area -->
<section class="common-shollership-area create-legacy-sup-res">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-4 pr-0 p-sm-0 p-xs-0">
                <div class="scholer-images">
                    <img src="img/create-legacy/endowed-space-image.webp" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-8 pl-0 p-sm-0 p-xs-0">
                <div class="scholer-conternt">
                   <h1> Endowed Space</h1>
                   <p>Habib University has an innovative and technologically advanced campus designed to meet the demands of the 21st century. Students are able to express their creativity and learn with the assistance of unique spaces. By endowing a space at the Habib University campus you can ensure that future generations of students can continue to avail a world-class education at a state-of-the-art facility.</p>
                   
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Common Scholership area -->


<!-- Make Gift -->
<?php include './include/make-gift.php' ?>
<!-- Make Gift -->

<!-- Footer -->
<?php include './include/footer.php' ?>
<!-- Footer -->
